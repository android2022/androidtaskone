package by.dazerty.taskone

import android.os.Bundle
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity

class MainActivity : AppCompatActivity() {
    lateinit var listOfLngs: LinearLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        listOfLngs = findViewById(R.id.listLanguages)

        listOf("England", "Belarus", "France", "Ukraine", "Czech republic", "Russia", "Netherlands", "Germany", "Filipino").forEach { s ->
            run {
                var textLng = TextView(this)
                textLng.apply {
                    text = s
                }

                listOfLngs.addView(textLng)
            }
        }
    }
}